<?php
include "bdd.php";

$query = $bdd->query("SELECT * FROM advert ORDER BY advert.id DESC LIMIT 15");

?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Le Bon Appart</title>
	<link rel="stylesheet" href="global.css">
</head>
<body>
	<nav>
		<ul>
			<li class="active"><a href="projet_bon_coin.php">Accueil</a></li>
			<li><a href="annonces_list.php">Consulter toutes les annonces</a></li>
			<li><a href="ajout_annonce.php">Ajouter une annonce</a></li>
		</ul>
	</nav>

	<h1>Le Bon Appart</h1>
	<div class="tableau">
		<p class="subtitle">Voici les 15 dernières annonces postées :</p>
		<table border="1">
			<thead>
				<tr>
					<th colspan="7">Les dernières annonces</th>
				</tr>
				<tr>
					<th>Titre</th>
					<th>Description</th>
					<th>Code postal</th>
					<th>Ville</th>
					<th>Type</th>
					<th>Prix</th>
					<th>Détails</th>
				</tr>
			</thead>
			<tbody>
			<?php
				while ($result = $query->fetch()) {
			?>
				<tr <?php if (!empty($result['reservation_message'])) { echo "style=\"background-color: grey;\"";}
						?>>
					<th><?php echo strtoupper($result['title'])?></th>
					<th><?php echo $result['description']?></th>
					<th><?php echo $result['postal_code']?></th>
					<th><?php echo $result['city']?></th>
					<th><?php echo $result['type']?></th>
					<th><?php echo $result['price']?></th>
					<th><a href="annonce.php?id=<?php echo $result['id'];?>">Consulter l'annonce</a></th>
				</tr>
			<?php
				}
			?>
			</tbody>
		</table>
	</div>
</body>
</html>
-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mar. 19 oct. 2021 à 11:09
-- Version du serveur :  10.3.31-MariaDB-0ubuntu0.20.04.1
-- Version de PHP : 7.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `projet_bon_coin`
--

-- --------------------------------------------------------

--
-- Structure de la table `advert`
--

CREATE TABLE `advert` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  `city` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `reservation_message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `advert`
--

INSERT INTO `advert` (`id`, `title`, `description`, `postal_code`, `city`, `type`, `price`, `reservation_message`) VALUES
(24, 'test1', 'test1', 'test1', 'test1', 'Location', 1.7, NULL),
(25, 'test2', 'test2', 'test2', 'test2', 'Vente', 1.3, NULL),
(26, 'test1', 'test1', 'test1', 'test1', 'Location', 1.7, NULL),
(27, 'test2', 'test2', 'test2', 'test2', 'Vente', 1.3, NULL),
(28, 'test3', 'test3', 'test3', 'test3', 'Location', 1, NULL),
(29, 'test4', 'test4', 'test4', 'test4', 'Vente', 1, NULL),
(30, 'test5', 'test5', 'test5', 'test5', 'Vente', 1, NULL),
(31, 'test6', 'test6', 'test6', 'test6', 'Location', 1, NULL),
(32, 'test7', 'test7', 'test7', 'test7', 'Location', 2, NULL),
(33, 'test8', 'test8', 'test8', 'test8', 'Location', 3, NULL),
(34, 'test9', 'test9', 'test9', 'test9', 'Vente', 4, NULL),
(35, 'test10', 'test10', 'test10', 'test10', 'Vente', 5, NULL),
(36, 'test11', 'test11', 'test11', 'test11', 'Location', 5, NULL),
(37, 'test12', 'test12', 'test12', 'test12', 'Vente', 6, NULL),
(38, 'test13', 'test13', 'test13', 'test13', 'Location', 6, NULL),
(39, 'test14', 'test14', 'test14', 'test14', 'Location', 6, NULL),
(40, 'test15', 'test15', 'test15', 'test15', 'Vente', 6, NULL),
(41, 'test17', 'test17', 'test17', 'test17', 'Location', 6, NULL),
(42, 'test18', 'test18', 'test18', 'test18', 'Vente', 6, NULL),
(43, 'test19', 'test19', 'test19', 'test19', 'Vente', 6, NULL),
(44, 'test20', 'test20', 'test20', 'test20', 'Vente', 6, NULL),
(45, 'test21', 'test21', 'test21', 'test21', 'Vente', 6, NULL),
(46, 'test22', 'test22', 'test22', 'test22', 'Location', 35, NULL),
(47, 'test23', 'test23', 'test23', 'test23', 'Location', 56, NULL),
(48, 'test3', 'test3', 'test3', 'test3', 'Location', 1, NULL),
(49, 'test4', 'test4', 'test4', 'test4', 'Vente', 1, NULL),
(50, 'test5', 'test5', 'test5', 'test5', 'Location', 1, NULL),
(51, 'test6', 'test6', 'test6', 'test6', 'Vente', 1, NULL),
(52, 'test7', 'test7', 'test7', 'test7', 'Location', 2, NULL),
(53, 'test8', 'test8', 'test8', 'test8', 'Vente', 3, NULL),
(54, 'test9', 'test9', 'test9', 'test9', 'Location', 4, NULL),
(55, 'test10', 'test10', 'test10', 'test10', 'Vente', 5, NULL),
(56, 'test11', 'test11', 'test11', 'test11', 'Location', 5, NULL),
(57, 'test12', 'test12', 'test12', 'test12', 'Vente', 6, NULL),
(58, 'test13', 'test13', 'test13', 'test13', 'Location', 6, NULL),
(59, 'test14', 'test14', 'test14', 'test14', 'Location', 6, NULL),
(60, 'test15', 'test15', 'test15', 'test15', 'Vente', 6, NULL),
(61, 'test17', 'test17', 'test17', 'test17', 'Vente', 6, NULL),
(62, 'test18', 'test18', 'test18', 'test18', 'Vente', 6, NULL),
(63, 'test19', 'test19', 'test19', 'test19', 'Location', 6, NULL),
(64, 'test20', 'test20', 'test20', 'test20', 'Vente', 6, NULL),
(65, 'test21', 'test21', 'test21', 'test21', 'Vente', 6, 'fsd'),
(66, 'test22', 'test22', 'test22', 'test22', 'Location', 35, 'fdsfs'),
(67, 'test23', 'test23', 'test23', 'test23', 'Location', 56, 'sds'),
(68, 'test24', 'test24', 'test24', 'test24', 'Vente', 5, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `advert`
--
ALTER TABLE `advert`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `advert`
--
ALTER TABLE `advert`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
	include "bdd.php";

	$id = $_GET['id'];

	$query = $bdd->query("SELECT * FROM advert WHERE id = $id");
	$result = $query->fetch();

	$title = $result['title'];
	$description = $result['description'];
	$city = $result['city'];
	$postal_code = $result['postal_code'];
	$type = $result['type'];
	$price = $result['price'];
	$reservation_message = $result['reservation_message'];


	if (!empty($_POST['submit'])) {
		$id = $_GET['id'];
		if (!empty($_POST['reservation_message'])){
			$reservation_message = $_POST['reservation_message'];
		}
		else {
			$reservation_message = null;
		}
		$update =  $bdd->query("UPDATE advert SET reservation_message = \"$reservation_message\" WHERE id = $id");
	}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Le Bon Appart</title>
	<link rel="stylesheet" href="global.css">
</head>
<body>
	<nav>
		<ul>
			<li><a href="index.php">Accueil</a></li>
			<li><a href="annonces_list.php">Consulter toutes les annonces</a></li>
			<li><a href="ajout_annonce.php">Ajouter une annonce</a></li>
		</ul>
	</nav>

	<h1>Le Bon Appart</h1>
	<div class="annonce_details">
		<p><strong> Titre : </strong> <?php echo $title ;?></p>
		<p><strong>description :</strong> <?php echo $description ;?></p>
		<p><strong>Adresse : </strong> <?php echo $city." ".$postal_code;?></p>
		<p><strong>type :</strong> <?php echo $type ;?></p>
		<p><strong>prix :</strong> <?php echo $price;?></p>
		<p><strong>Message de réservation :</strong> <?php if ($reservation_message != null) {
			echo $reservation_message; } else { echo "Cette annonce n'est pas réservée"; }?></p>
	</div>

	<?php if (empty($reservation_message)) { ?>
		<div class="form_resa">
			<form method="post" action="annonce.php?id=<?php echo $id;?>">
				<textarea name="reservation_message" id="reservation_message" cols="30" rows="5" required></textarea>
				<input type="submit" value="réserver">
			</form>
		</div>
	<?php
	} ?>

</body>
</html>
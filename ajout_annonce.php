<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Ajout d'une annonce</title>
	<link rel="stylesheet" href="global.css">
</head>
<body>
	<nav>
		<ul>
			<li><a href="index.php">Accueil</a></li>
			<li><a href="annonces_list.php">Consulter toutes les annonces</a></li>
			<li class="active"><a href="ajout_annonce.php">Ajouter une annonce</a></li>
		</ul>
	</nav>
	<h1>Ajouter une annonce</h1>

	<fieldset>

		<form method="post" action="ajout.php">

			<div class="title">
				<label for="title">Titre de votre annonce</label>
				<input type="text" name="title" id="title" required>
			</div>

			<div class="description">
				<label for="description">Description de votre annonce</label><br><br>
				<textarea name="description" id="description" cols="50" rows="13" required></textarea>
			</div>

			<div class="code_postal">
				<label for="code_postal">Votre code Postal</label>
				<input type="text" name="code_postal" id="code_postal" required>
			</div>

			<div class="city">
				<label for="city">Votre ville</label>
				<input type="text" name="city" id="city" required>
			</div>

			<div class="type">
				<label for="type">Type de l'annonce</label>
				<select name="type" id="type" required>
					<option value="Location">Location</option>
					<option value="Vente">Vente</option>
				</select>
			</div>

			<div class="price">
				<label for="price">Prix</label>
				<input type=number step=0.1 name="price" id="price" required>
			</div>

			<input type="submit" value="Ajouter">
		</form>
	</fieldset>

</body>
</html>